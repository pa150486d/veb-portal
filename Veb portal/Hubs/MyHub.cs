﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.SignalR;
using Veb_portal.Models;

namespace Veb_portal.Hubs
{
    public class MyHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }


        public  void CloseAuction(string auctionId)
        {
            VebPortalDb db = new VebPortalDb();
            Guid id = Guid.Parse(auctionId);
            Auction auction = db.Auctions.Single(field => field.Id == id);
            if(auction.State == Auction.COMPLETED)
                return;

            auction.State = Auction.COMPLETED;
            auction.ClosedAt = DateTime.Now;
            Bid bid = db.Bids.Where(field => field.AuctionId == id).OrderByDescending(field => field.SentAt).FirstOrDefault();
            if (bid != null)
                auction.WinnerId = bid.UserId;

            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            IEnumerable<string> allUsers = db.Bids.Where(m => ((m.AuctionId == id) && (m.UserId != bid.UserId))).Select(m => m.UserId).Distinct().ToList();

            foreach (var item in allUsers)
            {
                var lastBid = db.Bids.Where(m => m.UserId == item).OrderByDescending(m => m.SentAt).FirstOrDefault().TokenNumber;
                var user = userManager.FindById(item);
                user.TokenNumber += lastBid;
                userManager.Update(user);
            }
            
            db.SaveChanges();

            Clients.All.closeAuction(auctionId);        //SettingProductTimer script
            Clients.All.alertAboutClosed(auctionId);
        }


        public double GetLastBidOffer(string auctionId)
        {
            Guid id = Guid.Parse(auctionId);
            VebPortalDb db = new VebPortalDb();

            Bid bid = db.Bids.OrderByDescending(field => field.SentAt).FirstOrDefault(field => field.AuctionId == id);
            if (bid == null)
            {
                var price = db.Auctions.Single(field => field.Id == id).CurrentPrice;
                double tokenValue = Double.Parse(db.DefaultSystemValues.Single(field => "T".Equals(field.Name)).Value);
                double minVal = Math.Ceiling((double)price/tokenValue);
                return minVal+1;
            }

            return (double) (bid.TokenNumber+1);
        }


        /*public void SetNewBidValue(Guid auctionId, string offer, string username, string newPrice)
        {
            Clients.All.setLastBidOffer(auctionId.ToString("D"), offer, newPrice);    //BidsFormScript
            Clients.All.updateOffer(auctionId.ToString("D"), username, newPrice);     //AuctionIndex
            Clients.All.refreshBidsList(offer, username);       //AuctionDetailsScript
        }*/
    }
}
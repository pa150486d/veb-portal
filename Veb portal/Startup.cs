﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Veb_portal.Models;

[assembly: OwinStartupAttribute(typeof(Veb_portal.Startup))]
namespace Veb_portal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
            CreateRolesandUsers();
            InsertDefaultValuesAndPackages();
        }

        


        private void CreateRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User    
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool   
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole
                {
                    Name = "Admin"
                };
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website                  

                var user = new ApplicationUser
                {
                    FirstName = "Admin",
                    LastName = "Admin",
                    UserName = "admin@gmail.com",
                    Email = "admin@gmail.com"
                };

                string userPWD = "AdminPass1";
                var chkUser = userManager.Create(user, userPWD);

                //Add default User to Role Admin   
                if (chkUser.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Admin");

                }
            }
        }


        private void InsertDefaultValuesAndPackages()
        {
            VebPortalDb db = new VebPortalDb();
            if (db.DefaultSystemValues.ToArray().Length == 0)
            {
                db.Database.ExecuteSqlCommand(
                    @"insert into DefaultSystemValues(Id, Name, Value) values(NEWID(), 'N', 10)");
                db.Database.ExecuteSqlCommand(
                    @"insert into DefaultSystemValues(Id, Name, Value) values(NEWID(), 'D', 18000)");
                db.Database.ExecuteSqlCommand(
                    @"insert into DefaultSystemValues(Id, Name, Value) values(NEWID(), 'C', 'RSD')");
                db.Database.ExecuteSqlCommand(
                    @"insert into DefaultSystemValues(Id, Name, Value) values(NEWID(), 'T', 100)");
            }

            if (db.Packages.ToArray().Length == 0)
            {
                db.Database.ExecuteSqlCommand(
                    @"insert into Package(Id, Name, TokenNumber) values(NEWID(), 'Silver', 30)");
                db.Database.ExecuteSqlCommand(
                    @"insert into Package(Id, Name, TokenNumber) values(NEWID(), 'Gold', 50)");
                db.Database.ExecuteSqlCommand(
                    @"insert into Package(Id, Name, TokenNumber) values(NEWID(), 'Platinum', 100)");
            }
        }
    }
}

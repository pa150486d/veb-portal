using System.Web;

namespace Veb_portal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Auction")]
    public partial class Auction
    {
        public Guid Id { get; set; }

        [Required]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid duration value.")]
        public decimal Duration { get; set; }

        [Required]
        [Display(Name = "Start price")]
        [Range(0, Double.MaxValue, ErrorMessage = "The price must be bigger than 0.")]
        public decimal? StartPrice { get; set; }

        [Display(Name = "Current price")]
        public decimal? CurrentPrice { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? OpenedAt { get; set; }

        public DateTime? ClosedAt { get; set; }

        public int State { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "Product name")]
        public string ProductName { get; set; }

        [Display(Name = "Product picture")]
        public string ProductPicture { get; set; }

        
        [StringLength(18)]
        public string Currency { get; set; }

        [StringLength(128)]
        public string WinnerId { get; set; }

    }
}

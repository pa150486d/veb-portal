﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Veb_portal.Models.PartialClasses;

namespace Veb_portal.Models
{
    
    public partial class Auction
    {
        [NotMapped]
        public HttpPostedFileBase FileImage { get; set; } 
        public static int READY = 1;
        public static int OPENED = 2;
        public static int COMPLETED = 3;



        public string GetLeftAuctionTime()
        {
            if (State == READY)
            {
                return Operations.SecToTimeString((double)Duration);
            }

            if (State == OPENED)
            {
                TimeSpan ts1 = DateTime.Now.TimeOfDay;
                TimeSpan ts2 = OpenedAt.Value.TimeOfDay;
                TimeSpan time = ts1 - ts2;
                if ((TimeSpan.FromSeconds((double)Duration).Seconds - time.Seconds) <= 0)
                {
                    return "Ended";
                }
                else
                {
                    return Operations.SecToTimeString(time.TotalSeconds);
                }
            }

            return "Ended";
        }


       



        public string LastBidsUser()
        {
            VebPortalDb db = new VebPortalDb();

            Bid bid = db.Bids.OrderByDescending(field => field.SentAt).FirstOrDefault(field => field.AuctionId == Id);
            if(bid == null)
                return "No bid";

            return UserOperations.GetUsernameById(bid.UserId);
        }



        public string StateToString()
        {
            return State == OPENED ? "Opened" : (State == COMPLETED ? "Ended" : "Ready");
        }
    }


    public static partial class Operations
    {
        public static string SecToTimeString(double seconds)
        {
            int s = (int) seconds % 60;
            seconds = seconds / 60;
            int mins = (int)(seconds % 60);
            int hours = (int)(seconds / 60);
            string ss, mm, hh;

            ss = s <= 0 ? "0" + s : "" + s;
            mm = mins <= 0 ? "0" + mins : "" + mins;
            hh = hours <= 0 ? "0" + hours : "" + hours;
            return "" + hh + ':' + mm + ':' + ss;
        }
    } 
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Veb_portal.Models.PartialClasses
{
    public class UserOperations
    {
 
        public static string GetUsernameById(string id)
        {
            Task<string> username = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())).GetEmailAsync(id);
            return (username.Result).Split('@')[0];
        }

        public static ApplicationUser GetUserById(string id)
        {
            ApplicationUser user = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())).FindById(id);
            return user;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Veb_portal.Models.ModelViews
{
    public class DetailsAuctionModel
    {
        public Auction Auction { get; set; }

        public IEnumerable<Bid> ListOfBids { get; set; }

        public bool AuctionRequests = false;

        public int PageToReturn = 1;
    }
}
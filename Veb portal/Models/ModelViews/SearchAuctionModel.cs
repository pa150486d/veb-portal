﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using PagedList;


namespace Veb_portal.Models.ModelViews
{
    public class SearchAuctionModel
    {
        public IPagedList<Auction> PaginationResultList { get; set; } = null;

        public bool SearchActive { get; set; } = false;

        public string NameFilter { get; set; } = null;

        public double MinValueFilter { get; set; }

        public double MaxValueFilter { get; set; }

        public int StateFilter { get; set; } = 0;

        public bool WinnedAuctions { get; set; } = false;


        public IEnumerable<Auction> GetFilteredList(IQueryable<Auction>  data)
        {
            VebPortalDb db = new VebPortalDb();

            var userId = HttpContext.Current.User.Identity.GetUserId();
            if (WinnedAuctions != false)
            {
                data = data.Where(field => field.WinnerId == userId);
            }

            if (MinValueFilter > 0)
                data = data.Where(field => (double)field.CurrentPrice >= MinValueFilter);
            if (MaxValueFilter > 0)
                data = data.Where(field => (double)field.CurrentPrice <= MaxValueFilter);
            if (StateFilter != 0)
                data = data.Where(field => field.State == StateFilter);

            if (NameFilter != null)
            {
                string[] names = NameFilter.Split(' ');
                data = data.Where(field => names.Any(s => field.ProductName.Contains(s))); //moze da se koristi i indexOf metoda
            }

            return data.OrderByDescending(field => field.OpenedAt).ToList();
        }
    }
}
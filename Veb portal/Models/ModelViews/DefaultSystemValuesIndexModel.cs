﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Veb_portal.Models.ModelViews
{
    public class DefaultSystemValuesIndexModel
    {
        public IEnumerable<DefaultSystemValue> DefValues;
        public IEnumerable<Package> Packages;
    }
}
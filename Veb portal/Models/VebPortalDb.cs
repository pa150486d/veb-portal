namespace Veb_portal.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class VebPortalDb : DbContext
    {
        public VebPortalDb()
            : base("name=VebPortalDb")
        {
        }

        public virtual DbSet<Auction> Auctions { get; set; }
        public virtual DbSet<Bid> Bids { get; set; }
        public virtual DbSet<DefaultSystemValue> DefaultSystemValues { get; set; }
        public virtual DbSet<Package> Packages { get; set; }
        public virtual DbSet<TokenOrder> TokenOrders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Auction>()
                .Property(e => e.Duration)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Auction>()
                .Property(e => e.Currency)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}

namespace Veb_portal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TokenOrder")]
    public partial class TokenOrder
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public decimal? Price { get; set; }

        public int State { get; set; }

        public Guid? PackageId { get; set; }

        public virtual Package Package { get; set; }

        public DateTime? CreatedAt { get; set; }

        public decimal? TokenNumber { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Veb_portal.Controllers.VPControllers
{
    public class Emails
    {

        public void EmailNotifications(string address, string subject, string body)
        {
            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            MailMessage mail = new MailMessage();

            mail.To.Add(new MailAddress(address));
            mail.Subject = subject;
            mail.Body = body;

            mail.From = new MailAddress("bid1st.pa150486d@gmail.com", "Bid1st - online auction portal");
            smtpServer.Port = 587;
            smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpServer.UseDefaultCredentials = false;
            smtpServer.Credentials = new NetworkCredential("bid1st.pa150486d@gmail.com", "pa150486d");
            smtpServer.EnableSsl = true;

            smtpServer.Send(mail);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using Veb_portal.Models;
using Veb_portal.Models.ModelViews;

namespace Veb_portal.Controllers.VPControllers
{
    public class AuctionsController : Controller
    {
        private VebPortalDb db = new VebPortalDb();


        // GET: Auctions
        public ActionResult Index(int? page, SearchAuctionModel model)
        {
            IQueryable<Auction> query = db.Auctions.Where(field => field.State != Auction.READY);
            return View(AuctionsPagedList(page, model, query));
        }



        // POST: Auctions
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(SearchAuctionModel model)
        {
            model.SearchActive = true;

            return Index(1, model);
        }

        // GET: Auctions/Details/5
        public ActionResult Details(Guid? id, int page, bool? auctionRequestsList, bool? makeBid)
        {
            makeBid = (makeBid ?? false);
            if (makeBid == true && !User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }
            
            var model = new DetailsAuctionModel()
            {
                Auction = auction,
                ListOfBids = db.Bids.Where(field => field.AuctionId == auction.Id).OrderByDescending(field => field.SentAt).ToList(),
                AuctionRequests = (auctionRequestsList ?? false),
                PageToReturn = page
            };

            return View(model);
        }

        [Authorize]
        // GET: Auctions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Auctions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Auction auction)
        {
            if (ModelState.IsValid)
            {
                auction.Id = Guid.NewGuid();
                
                string fileName = System.IO.Path.Combine(Server.MapPath("~/Images"), auction.Id.ToString("N") + ".jpg");
                auction.FileImage.SaveAs(fileName);
                auction.ProductPicture = "../../Images/" + auction.Id.ToString("N") + ".jpg";

                auction.CurrentPrice = auction.StartPrice;
                auction.CreatedAt = DateTime.Now;
                auction.State = Auction.READY;
                auction.Currency = db.DefaultSystemValues.Single(s => "C".Equals(s.Name)).Value;

                db.Auctions.Add(auction);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", "Auctions");
        }


        public SearchAuctionModel AuctionsPagedList(int? page, SearchAuctionModel model, IQueryable<Auction> query)
        {
            IEnumerable<Auction> data;

            if (model.SearchActive == false)
            {
                data = query.OrderByDescending(field => field.OpenedAt).ToList();
            }
            else
            {
                data = model.GetFilteredList(query);
            }
                

            var pageSize = Int32.Parse(db.DefaultSystemValues.Single(s => "N".Equals(s.Name)).Value);   //treba da se dohvati podrazumevani broj po strani
            var pageNumber = (page ?? 1);
            model.PaginationResultList = data.ToPagedList(pageNumber, pageSize);

            return model;
        }



        [AdminAuthorize(Roles = "Admin")]
        //GET:Auction/AuctionRequests
        public ActionResult AuctionRequests(int? page, SearchAuctionModel model)
        {
            IQueryable<Auction> query = db.Auctions.Where(field => field.State == Auction.READY);

            return View(AuctionsPagedList(page, model, query));    
        }

        
        // POST: Auctions
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminAuthorize(Roles = "Admin")]
        public ActionResult AuctionRequests(SearchAuctionModel model)
        {
            model.SearchActive = true;

            return AuctionRequests(1, model);
        }





        [AdminAuthorize(Roles = "Admin")]
        public ActionResult OpenAuction(int page, Guid aucId, bool searchActive, string nameFilter, double minValueFilter, double maxValueFilter )
        {
            SearchAuctionModel toPass = new SearchAuctionModel
            {
                SearchActive = searchActive,
                NameFilter = nameFilter,
                MinValueFilter = minValueFilter,
                MaxValueFilter = maxValueFilter
            };
            Auction auction = db.Auctions.Single(field => field.Id == aucId);
            
            auction.State = Auction.OPENED;
            auction.OpenedAt = DateTime.Now;
            db.SaveChanges();
            return RedirectToAction("AuctionRequests", new{page, toPass });
        }
        
        /*[AdminAuthorize(Roles = "Admin")]
        public ActionResult OpenAuction(double openedAt, int page, string id, string searchActive, string nameFilter, double minValueFilter, double maxValueFilter )
        {
            Guid aucId = Guid.Parse(id);
            SearchAuctionModel toPass = new SearchAuctionModel
            {
                SearchActive = Int32.Parse(searchActive) == 0? false: true,
                NameFilter = nameFilter,
                MinValueFilter = minValueFilter,
                MaxValueFilter = maxValueFilter
            };
            Auction auction = db.Auctions.Single(field => field.Id == aucId);
            
            auction.State = Auction.OPENED;
            
            auction.OpenedAt = (new DateTime().AddMilliseconds(openedAt));      //jedino sto je promenjeno

            db.SaveChanges();
            return RedirectToAction("AuctionRequests", new{page, toPass });
        }*/

        [AdminAuthorize(Roles = "Admin")]
        public ActionResult DeleteAuction(int page, Guid aucId, [Bind(Include = "StateFilter,NameFilter, MinValueFilter, MaxValueFilter ")] SearchAuctionModel toPass)
        {
            Auction auction = db.Auctions.Single(field => field.Id == aucId);
            if (auction != null)
            {
                db.Auctions.Remove(auction);
                db.SaveChanges();
            }
            
            return RedirectToAction("AuctionRequests", new { page, toPass });
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}




/*
        // GET: Auctions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }
            return View(auction);
        }

        // POST: Auctions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Duration,StartPrice,CurrentPrice,CreatedAt,OpenedAt,ClosedAt,State,ProductName,ProductPicture,Currency,WinnerId")] Auction auction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(auction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(auction);
        }

        // GET: Auctions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }
            return View(auction);
        }

        // POST: Auctions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Auction auction = db.Auctions.Find(id);
            db.Auctions.Remove(auction);
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/

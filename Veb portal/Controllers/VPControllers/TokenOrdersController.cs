﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;
using Veb_portal.Models;
using Veb_portal.Models.ModelViews;
using Veb_portal.Models.PartialClasses;

namespace Veb_portal.Controllers.VPControllers
{
    [UserAuthorize]
    public class TokenOrdersController : Controller
    {
        public static int SUBMITTED = 1;
        public static int CANCELED = 2;
        public static int COMPLETED = 3;

        private VebPortalDb db = new VebPortalDb();

        // GET: TokenOrders
        public ActionResult Index(int? page)
        {
            string userId = User.Identity.GetUserId();
            var tokenOrders = db.TokenOrders.Where(field => field.UserId == userId).OrderByDescending(field => field.CreatedAt).ToList();
            var pageSize = Int32.Parse(db.DefaultSystemValues.Single(s => "N".Equals(s.Name)).Value);   //treba da se dohvati podrazumevani broj po strani
            var pageNumber = (page ?? 1);
            IPagedList<TokenOrder> model = tokenOrders.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        

        // GET: TokenOrders/Create
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.PackageId = new SelectList(db.Packages, "Id", "Name");
            var packages = db.Packages.OrderBy(m => m.TokenNumber).ToArray();
            TokenOrderModel model = new TokenOrderModel
            {
                Packages = packages
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PassOrder()
        {
            TokenOrder order = new TokenOrder
            {
                Id = Guid.NewGuid(),
                UserId = User.Identity.GetUserId(),
                State = SUBMITTED
            };

            var package = Request["PackageChoice"];
            double tokNum = Double.Parse(Request["TokenNumber"]);
            order.PackageId = Guid.Parse(package);

            tokNum = tokNum * (double)db.Packages.Single(m => m.Id == order.PackageId).TokenNumber;
            double price = tokNum * Double.Parse(db.DefaultSystemValues.Single(m => m.Name.Equals("T")).Value);
            order.TokenNumber = (decimal?)tokNum;

            order.Price = (decimal?)price;

            return View("ConfirmOrder", order);
        }



        public ActionResult ConfirmOrder([Bind(Include = "id, Price, TokenNumber, PackageId")]TokenOrder data)
        {
            data.CreatedAt = DateTime.Now;
            data.State = SUBMITTED;
            data.UserId = User.Identity.GetUserId();
            db.TokenOrders.Add(data);
            db.SaveChanges();

            return Redirect("http://stage.centili.com/payment/widget?apikey=8603e733aca172f0f2472ecadffb5739&country=rs&reference="+data.Id+ "&returnurl=http://bid1st-pa150486d.azurewebsites.net/TokenOrders/PaymentCompleted");
        }



        

        public ActionResult PaymentCompleted(Guid? reference, string status)
        {
            //Guid orderId = Guid.Parse(clientid);
            TokenOrder order = db.TokenOrders.Single(m => m.Id == reference);

            if (order == null)
            {
                return RedirectToAction("Index", "TokenOrders");
            }

            string message = "";
            if (status == "success")
            {    
                message = "Your order is successfuly completed!";
                order.State = COMPLETED;
                UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var user = userManager.FindById(User.Identity.GetUserId());
 
                user.TokenNumber += (order.TokenNumber ?? 0);
                userManager.Update(user);
                db.SaveChanges();

                Emails sendMail = new Emails();
                sendMail.EmailNotifications(user.Email, "Token order completed", "Thank you! Your order is completed successfully.");
            }
            else
            {
                order.State = CANCELED;
                if (status == "failed")
                {
                    message = "Your order failed! Try again later.";
                }
                else
                {
                    message = "Your order is canceled!";
                }
            }

            ViewBag.OrderMessage = message;

            return RedirectToAction("Index", "TokenOrders");
        }


        public static string StateToString(int state)
        {
            if (state == 1)
                return "Submited";
            if(state == 2)
                return "Canceled";
            if(state == 3)
                return "Completed";
            return "Waiting";
        }



        // POST: TokenOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        /*  [HttpPost]
          [ValidateAntiForgeryToken]
          public ActionResult Create(TokenOrderModel tokenOrder)
          {
              if (ModelState.IsValid)
              {
                  tokenOrder.Id = Guid.NewGuid();
                  db.TokenOrders.Add(tokenOrder);
                  db.SaveChanges();
                  return RedirectToAction("Index");
              }

              ViewBag.PackageId = new SelectList(db.Packages, "Id", "Name", tokenOrder.PackageId);
              return View(tokenOrder);
          }*/




        /*
        // GET: TokenOrders/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TokenOrder tokenOrder = db.TokenOrders.Find(id);
            if (tokenOrder == null)
            {
                return HttpNotFound();
            }
            return View(tokenOrder);
        }


        // GET: TokenOrders/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TokenOrder tokenOrder = db.TokenOrders.Find(id);
            if (tokenOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.PackageId = new SelectList(db.Packages, "Id", "Name", tokenOrder.PackageId);
            return View(tokenOrder);
        }

        // POST: TokenOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Price,State,PackageId")] TokenOrder tokenOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tokenOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PackageId = new SelectList(db.Packages, "Id", "Name", tokenOrder.PackageId);
            return View(tokenOrder);
        }

        // GET: TokenOrders/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TokenOrder tokenOrder = db.TokenOrders.Find(id);
            if (tokenOrder == null)
            {
                return HttpNotFound();
            }
            return View(tokenOrder);
        }

        // POST: TokenOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            TokenOrder tokenOrder = db.TokenOrders.Find(id);
            db.TokenOrders.Remove(tokenOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

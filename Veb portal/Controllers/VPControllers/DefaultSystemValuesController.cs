﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Veb_portal.Models;
using Veb_portal.Models.ModelViews;

namespace Veb_portal.Controllers.VPControllers
{
    [AdminAuthorize(Roles = "Admin")]
    public class DefaultSystemValuesController : Controller
    {
        private VebPortalDb db = new VebPortalDb();

        // GET: DefaultSystemValues
        public ActionResult Index()
        {
            DefaultSystemValuesIndexModel model = new DefaultSystemValuesIndexModel
            {
                DefValues = db.DefaultSystemValues.ToList(),
                Packages = db.Packages.ToList()
            };
            return View(model);
        }

      /*  // GET: DefaultSystemValues/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultSystemValue defaultSystemValue = db.DefaultSystemValues.Find(id);
            if (defaultSystemValue == null)
            {
                return HttpNotFound();
            }
            return View(defaultSystemValue);
        }*/

        // GET: DefaultSystemValues/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DefaultSystemValues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Value")] DefaultSystemValue defaultSystemValue)
        {
            if (ModelState.IsValid)
            {
                defaultSystemValue.Id = Guid.NewGuid();
                db.DefaultSystemValues.Add(defaultSystemValue);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(defaultSystemValue);
        }

        // GET: DefaultSystemValues/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultSystemValue defaultSystemValue = db.DefaultSystemValues.Find(id);
            if (defaultSystemValue == null)
            {
                return HttpNotFound();
            }
            return View(defaultSystemValue);
        }

        // POST: DefaultSystemValues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Value")] DefaultSystemValue defaultSystemValue)
        {
            if (ModelState.IsValid)
            {
                db.Entry(defaultSystemValue).State = EntityState.Modified;
                string[] package = { "S", "G", "P"};
                if (package.Contains(defaultSystemValue.Name))
                {

                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(defaultSystemValue);
        }

        // GET: DefaultSystemValues/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultSystemValue defaultSystemValue = db.DefaultSystemValues.Find(id);
            if (defaultSystemValue == null)
            {
                return HttpNotFound();
            }
            return View(defaultSystemValue);
        }

        // POST: DefaultSystemValues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            DefaultSystemValue defaultSystemValue = db.DefaultSystemValues.Find(id);
            db.DefaultSystemValues.Remove(defaultSystemValue);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }


    
}

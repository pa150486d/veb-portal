﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.SignalR;
using Veb_portal.Hubs;
using Veb_portal.Models;
using Veb_portal.Models.PartialClasses;

namespace Veb_portal.Controllers.VPControllers
{
    public class BidsController : Controller
    {
        private VebPortalDb db = new VebPortalDb();


        [HttpPost]
        public  JsonResult MakeABid(double offer, string auctionId)
        {
            Guid id = Guid.Parse(auctionId);
            var userId = User.Identity.GetUserId();

            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = UserManager.FindById(userId);



            Bid lastBidid = db.Bids.Where(field => (field.AuctionId == id && field.UserId == user.Id)).OrderByDescending(field => field.SentAt).FirstOrDefault();
            double tokenToTake = lastBidid != null ? offer - (double) lastBidid.TokenNumber : offer; 

            if (user.TokenNumber < (decimal) tokenToTake)
                return Json(-1);
            
            user.TokenNumber -= (decimal) tokenToTake;
            UserManager.Update(user);

            double newPrice = offer * Double.Parse(db.DefaultSystemValues.Single(field => "T".Equals(field.Name)).Value);

            Bid newBid = new Bid
            {
                Id = Guid.NewGuid(),
                AuctionId = id,
                SentAt = DateTime.Now,
                TokenNumber = (decimal)offer,
                UserId = userId
            };
            db.Bids.Add(newBid);
            Auction auction = db.Auctions.Single(field => field.Id == id);
            auction.CurrentPrice = (decimal?)newPrice;
            db.SaveChanges();

            var hub= GlobalHost.ConnectionManager.GetHubContext<MyHub>();
            string username = User.Identity.Name;
            username = username.Split('@')[0]; //get part of mail before @ as username
            string offerStr = offer.ToString("F");

            hub.Clients.All.setLastBidOffer(id.ToString("D"), offerStr, newPrice + " " + auction.Currency);    //BidsFormScript
            hub.Clients.All.updateOffer(id.ToString("D"), username, newPrice + " " + auction.Currency);     //AuctionIndex
            hub.Clients.All.refreshBidsList(offerStr, username);       //AuctionDetailsScript

            string[] jsonRes = { newPrice + " " + auction.Currency, username};
            return Json(jsonRes);
        }



        /*
        // GET: Bids
        public ActionResult Index()
        {
            return View(db.Bids.ToList());
        }

        // GET: Bids/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bid bid = db.Bids.Find(id);
            if (bid == null)
            {
                return HttpNotFound();
            }
            return View(bid);
        }

        // GET: Bids/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Bids/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,AuctionId,SentAt,TokenNumber")] Bid bid)
        {
            if (ModelState.IsValid)
            {
                bid.Id = Guid.NewGuid();
                db.Bids.Add(bid);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bid);
        }

        // GET: Bids/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bid bid = db.Bids.Find(id);
            if (bid == null)
            {
                return HttpNotFound();
            }
            return View(bid);
        }

        // POST: Bids/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,AuctionId,SentAt,TokenNumber")] Bid bid)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bid).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bid);
        }

        // GET: Bids/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bid bid = db.Bids.Find(id);
            if (bid == null)
            {
                return HttpNotFound();
            }
            return View(bid);
        }

        // POST: Bids/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Bid bid = db.Bids.Find(id);
            db.Bids.Remove(bid);
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿var OPENED = 2;

$(document).ready(function () {
    var product = $('#productTimer');
    var value = product.html();
    value = value.split('*'); //{[0]OpenedAt, [1]Duration, [2]State, [3]Id}

    if (parseInt(value[2]) === OPENED) {
        console.log(value);
        setTimer($('#' + value[3]), value[0], value[1], value[3]);
    } 
    else {
        $('#' + value[3]).text("Ended");
    }
});


function refreshBidsList(offer, username) {
    var content = document.getElementById("bidsTableList");

    var bid = document.createElement("tr");
    bid.setAttribute("class", "bidItem");

    var user = document.createElement("th");
    username = document.createTextNode(username);
    user.appendChild(username);
    bid.appendChild(user);

    var offerDiv = document.createElement("th");
    offerDiv.setAttribute("class", "tokenNumCol");
    offer = document.createTextNode(parseFloat(offer).toFixed(2).toString());
    offerDiv.appendChild(offer);
    bid.appendChild(offerDiv);
    
    content.insertBefore(bid, content.firstChild);


}

$.connection.myHub.client.refreshBidsList = refreshBidsList;
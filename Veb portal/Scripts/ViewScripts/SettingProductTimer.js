﻿

function setTimer(product, openedAt, duration, auctionId) {
    if (product.html() === "Ended")
        return;

    var today = new Date();
    var leftTime = parseFloat(duration) * 1000 - (today.getTime() - Date.parse(openedAt)) + 7200*1000;  //vreme na serveru je drugacije
    leftTime = leftTime <= 0 ? 0 : leftTime;

    
    if (leftTime === 0) {
            if (product.html() === "Ended")
                return;
            myHub.done(function() {
                    $.connection.myHub.server.closeAuction(auctionId)
                    .done(function () { product.text("Ended"); });
            });   
    } else {
        product.text("" + msToTime(leftTime));
        setTimeout(setTimer, 1000, product, openedAt, duration, auctionId);
    }
}



function closeAuction(auctionId) {
    $("#" + auctionId).text = "Ended";
    $("#" + "bidButton-" + auctionId).addClass("btnDisable");
}

$.connection.myHub.client.closeAuction = closeAuction;



function msToTime(duration) {
    var seconds = parseInt(duration / 1000 % 60);
    var minutes = parseInt(duration / (1000 * 60) % 60);
    var hours = parseInt(duration / (1000 * 60 * 60));

    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    return hours + ":" + minutes + ":" + seconds;
}
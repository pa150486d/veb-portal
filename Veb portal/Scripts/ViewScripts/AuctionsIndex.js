﻿
$("#searchForm").submit(function (event) {
    var min = $("#minVal").val();
    var max = $("#maxVal").val();
    min = min.parseFloat();
    max = max.parseFloat();
    if (min !== 0 && max !== 0) {
        if (min > max) {

            $("#MinMaxErrorMessage").text("Min value can't be greater than max value.");
            event.preventDefault();
        } else
            event.submit();
    }

});

var OPENED = 2;

$(document).ready(function() {
    $(".productTimer").each(function () {
        var value = $(this).html();
        value = value.split('*'); //{[0]OpenedAt, [1]Duration, [2]State, [3]Id}

        if (parseInt(value[2]) === OPENED)
            setTimer($('#' + value[3]), value[0], value[1], value[3]);
        else {
            $('#'+value[3]).text("Ended");
        }
    });
});


function updateOffer(auctionId, username, newPrice) {
    var tn1 = document.createTextNode(""+username);
    var lastBid = document.getElementById("lastBid-" + auctionId);
    if (lastBid != null) {
        lastBid.removeChild(lastBid.firstChild);
        lastBid.appendChild(tn1);
    } else
        return;

    var tn2 = document.createTextNode(""+newPrice);
    var price = document.getElementById("currentPrice-" + auctionId);
    if (price != null) {
        price.removeChild(price.firstChild);
        price.appendChild(tn2);
    }
}

$.connection.myHub.client.updateOffer = updateOffer;





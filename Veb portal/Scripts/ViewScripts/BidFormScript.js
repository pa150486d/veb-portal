﻿
$(window).on("load", function () {
    myHub.done(function() {
        $.connection.myHub.server.getLastBidOffer(auctionId)
            .done(function(offer) {
                offer = (offer === 0 ? "No bids" : "" + offer);
                $("#minBidVal").text(parseFloat(offer).toFixed(2).toString());
            });
    });
});

function setLastBidOffer(id, offer, newPrice) {
    if (auctionId === id) {
        offer = parseFloat(offer) + 1;
        $("#minBidVal").text(offer.toFixed(2).toString());
        $("#currentPrice").text(newPrice);
    }
}

$.connection.myHub.client.setLastBidOffer = setLastBidOffer;



$("#bidButton").click(function () {
    var offer = parseFloat($("#offer").val());
    var min = parseFloat($("#minBidVal").html());

    if (offer < min)
        $("#bidErrorMessage").text("Your offer is lower than minimal!");
    else {
        $.ajax({
            url: "/Bids/MakeABid",
            method:'POST',
            data: {
                offer: offer,
                auctionId: auctionId
            },
            dataType: "json",
            success: function (data) {
                if (data !== -1) {
                    //setLastBidOffer(auctionId, offer, data[0]);
                    //refreshBidsList(offer, data[1]);
                    return;
                }
                alert("You don't have enough tokens!");
            }
        });
    }
});